######################
#GitMyStuff Installer#
#2017 Varun Ramani   #
######################
import sys
import os
import platform
from subprocess import call as c
comm = ''
s = sys.platform
print("GitMyStuff Installer: 2017 Varun Ramani\n\n")
def prepareInstall():
    global comm
    global s
    print("Now checking operating system...")
    if ('linux' in s):
        s = platform.linux_distribution()[0]
        if (s.lower()=='ubuntu'):
            comm='sudo apt-get install git'
        if (s.lower()=='debian'):
            comm='sudo apt-get install git'
        if (s.lower()=='fedora'):
            comm='yum -y install git'
        if (s.lower()=='arch'):
            comm='pacman -Syu git'
    if ('darwin' in s):
        s='Darwin/OSX'
        comm='DARWIN INSTALL'
    if ('win' in s and 'cyg' not in s):
        s='Windows'
        comm='WINDOWS INSTALL'
    if ('cygwin' in s):
        s='Cygwin'
        comm='CYGWIN INSTALL'
    ok=False
    while (ok==False):
        if(input("Install for " + s + "?(y/n)")!='y'):
            choice = input("Available operating systems:\n\t1.Windows\n\t2.Ubuntu\n\t3.Debian\n\t4.Fedora\n\t5.Arch\n\t6.Other Linux\n\t7.Darwin/OSX\n\t8.Cygwin\nEnter your choice[default : 2]===>")
            if(choice=='1'):
                s='Windows'
            if(choice=='2'):
                s='Ubuntu'
            if(choice=='3'):
                s='Debian'
            if(choice=='4'):
                s='Fedora'
            if(choice=='5'):
                s='Arch'
            if(choice=='6'):
                s='Other Linux'
            if (choice=='7'):
                s='Darwin/OSX'
            if (choice=='8'):
                s='Cygwin'
            if (s.lower()=='ubuntu'):
                comm='sudo apt-get install'
            if (s.lower()=='debian'):
                comm='sudo apt-get install'
            if (s.lower()=='fedora'):
                comm='yum -y install'
            if (s.lower()=='arch'):
                comm='pacman -Syu'
            if (s.lower()=='other linux'):
                comm=input("Installation command(For Debian: sudo apt-get install)>")
            print("Set OS to " + s)
        if (s != 'Windows' and s != 'Darwin/OSX' and s != 'Cygwin'):
            print("Using command '" + comm + "'")
            if (input('Is this command OK?(y/n)')=='n'):
                comm=input('Enter your custom command>')
                print("Set command to " + comm)
        ok=(input("Install on '" + s + "' using install command '" + comm + "'? (y/n)")=='y')
        if (ok==False):
            print("Restarting selection...")
def LinuxInstall():
    global comm
    print("Installing GitMyStuff...")
    c(comm + " git", shell=True)
    c("chmod +x linux/setup.sh", shell=True)
    c("./linux/setup.sh", shell=True)
def WindowsInstall():
    print("Installing GitMyStuff...")
    c("windows/setup.bat", shell=True)
def CygwinInstall():
    print("Installing GitMyStuff")
    c("./cygwin/setup.bat", shell=True)
def DarwinInstall():
    print("Installing GitMyStuff...")
    c("chmod +x mac/setup.sh", shell=True)
    c("./mac/setup.sh", shell=True)
try:
    prepareInstall()
    if (s != 'Windows' and s != 'Darwin/OSX' and s != 'Cygwin'):
        LinuxInstall()
    if (s == 'Windows'):
        WindowsInstall()
    if (s == 'Darwin/OSX'):
        DarwinInstall()
    if (s == 'Cygwin'):
        CygwinInstall()
except:
    print("There was an error.\nIt is most likely that invalid input was supplied.")

    
    



    

