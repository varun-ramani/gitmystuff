# README #

### What is this repository for? ###
GitMyStuff is a wrapper around Git and (later) Subversion. It provides powerful convenience features not normally available in Git - and also strips down the amount of needless commands and work a developer must do. 

### How do I get set up? ###
YOU WILL NEED PYTHON3

## Linux ##
The power of Unix makes setting up GitMyStuff extremely easy. 
As a priveleged user (or with sudo on Debian) run "python3 install.py"

## Mac ##
Repeat above steps (make sure you are su)

## Windows ##
Are you serious? Windows? Get Linux or a Mac. If you don't really want to,
you can use a tool known as Cygwin. In Cygwin:

"cygstart --action=runas python3 install.py"

### Who do I talk to? ###
Questions or comments?
Email me at varun.ramani@gmail.com
