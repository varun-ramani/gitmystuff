#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "help.h"
#include "job.h"
int job(int argc, char *argva[]) {
	char* mode = argva[2];
	
	if (strcmp(mode, "--read")==0) {
		if (argc == 4) {
			printf("Now reading '%s'\n", argva[3]);
		} else {
			printf("Command syntax error.\n");
		}
	} else if (strcmp(mode, "--run")==0) {
		if (argc == 4) {
			printf("Running job '%s'\n", argva[3]);
		} else {
			printf("Command syntax error.\n");
		}
	}
}
