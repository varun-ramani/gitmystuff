#include "config.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <ctype.h>
config::config() {
    std::ifstream file(CONFIG);
    std::string string1;
    std::string string2;
    int onstring = 1;
    char c;
    bool inquote = false;
    while (file.get(c)) {
        if (onstring==1) {
            if(!(isspace(c)) {
                if (c != '=') {
                    string1 += c;
                }
                else {
                    onstring = 2;
                }
            }
        }
        if (onstring==2) {
            if(c != '\n') {
                if (inquote && c != '"') {
                    string2 += c;
                }
                if (c == '"') {
                    inquote = !inquote;
                }

            }
        }
    }
}
string config::getGit() {
    return data.git;
}
int config::getDebug() {
    return data.debug;
}
