#include <fstream>
#include <string>
class config {
    public:
        config();
        string getGit();
        int getDebug();
    private:
        struct data {
            string git;
            int debug;
        };
}