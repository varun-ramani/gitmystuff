#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "help.h"
#include "upload.h"


int upload(int args, char *argva[]) {
	char toreturn = 0;
	if (DEBUG) {
		printf("Found %d/5 needed arguments.\n", args);
		
	}
	//Init arrays/variables.
	char git[7000];
	char add[1000];
	char commit[1000];
	char push[1000];
	char gitadd[8000];
	char gitcommit[2000];
	char gitpush[2000];
	char force;
	
	//Get input from user - do they want to force the push to the Git repository?
	printf("Force push?(y/n)==>");
	scanf("%c", &force);
	
	//Create all necessary substrings strings
	sprintf(git, "%s", GIT);	
	sprintf(add, "add -v .");
	sprintf(commit, "commit -m '%s'", argva[4]);
	if (force=='y') {
		sprintf(push, "push -f %s %s", argva[2], argva[3]);
	} else {
		sprintf(push, "push %s %s", argva[2], argva[3]);
	}
	
	//Cocatnate substrings into a command
	sprintf(gitadd, "%s %s", git, add);
	sprintf(gitcommit, "%s %s", git, commit);
	sprintf(gitpush, "%s %s", git, push);
	
	
	//Execute commands and check if they were successful.
	printf("\nAdding...\n");
	if (!system(gitadd)) {
		printf("Successfully added everything.\n");
	} else {
		printf("Failed to add everything.\n");
		toreturn = 1;
	}
	printf("Committing...\n");
	if (!system(gitcommit)) {
		printf("Successfully commmitted everything.\n");
	} else {
		printf("Failed to commit everything.\n");
		toreturn = 1;
	}
	printf("Pushing...\n");
	if (!system(gitpush)) {
		printf("Successfully pushed everything.\n");
	} else {
		printf("Failed to push everything.\n");
		toreturn = 1;
	}
	
	//Check if the entire operation was successful.
	if (toreturn) {
		printf("OPERATION FAILED.\n\t1. Check where there was failure above, and read the messages from Git.\n\t2. Run gitmystuff help upload, and check your command syntax.\n");
	}
	return toreturn;
	
}
