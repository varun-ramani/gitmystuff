#include <stdio.h>
#include <string.h>
#include "help.h"
int help(int args, char *argv[]) {
	if (DEBUG) {
		printf("Reached Help\n");
		printf("Args=%d\n", args);
		for (int i = 0; i < args; i++) {
			printf("%s ", argv[i]);
		} printf("\n");
	}
	if (args==3) {
		if (DEBUG) {
			printf("Reached Checking Argument Phase\n");
		}
		char* about;
		sprintf(about, "%s", argv[2]);
		if (strcmp(about, "clone") == 0) {
			printf("\nThe clone command downloads a repository.\n\n\tusage: gitmystuff clone <repository_to_clone> <directory>\n\n\t<repository_to_clone>: Provide full repository URL.\n\t<directory>: Provide the full path of the directory you wish to clone into.\n\n\tExample:\n\tgitmystuff clone https://Xtraloudninja@bitbucket.org/Xtraloudninja/gitmystuff.git\n\n");

		} else if (strcmp(about, "init") == 0) {
			printf("\nThe init command initalizes Git in a repository.\n\n\tusage: gitmystuff init\n\n\tExample:\n\tgitmystuff init\n\n");

		} else if (strcmp(about, "upload") == 0) {
			printf("\nThe upload command adds everything, commits it, and then pushes it.\n\n\tusage: gitmystuff upload <remote> <branch> <commit_message>\n\n\t<remote>: Remote to upload to.\n\t<branch>: Branch to commit.\n\t<commit_message>: Describes commit\n\n\tExample:\n\tgitmystuff upload origin master 'Updated Foo.c'\n\n");

		} else if (strcmp(about, "help")==0) {
			printf("\nThe help command provides information about a specific command.\n\n\tusage: gitmystuff help <command>\n\n\t<command>: Command you want help with\n\n\tExample:\n\tgitmystuff help clone\n\n");
		} else if (strcmp(about, "uninit")==0) {
			printf("\nThe uninit command un-initializes Git.\n\n\tusage: gitmystuff uninit\n\nExample:\n\tgitmystuff uninit\n\n");
		} else {
			printf("\n'%s' is not a valid command.\n\n", about);
		}
	} else {
		if (DEBUG) {
			printf("Reached Plan B Phase\n");
		}
		if (args==1) {
			printf("GitMyStuff\n==========\n");
			printf("usage: gitmystuff <command> [<args>]\n\nCommands:\n\tclone - Clones a repository\n\tinit - Initalizes empty repository\n\tupload - Uploads to the remote\n\tuninit - Removes Git from the working directory\n\thelp - get help with a command.\nTry running gitmystuff help help\n\n");
		} else {
			printf("\nError: Invalid Arguments\n\n");
		}
	}
}
