#include <stdio.h>
#include <stdlib.h>
#include "dep/help.h"
#include "dep/clone.h"
#include "dep/init.h"
#include "dep/upload.h"
#include "dep/job.h"
#include "dep/uninit.h"
#include "dep/config.h"
#include <string>
using namespace std;
int main(int argc, char *argv[]) {
	config conf;
	int DEBUG = conf.getDebug();
	string GIT = conf.getGit();
	if (DEBUG) {
		printf("Found %d arguments.\n", argc);
		printf("Git set to '%s'\n", GIT);
	}
	if (argc <= 1) {
		help(argc, argv);
	} else if (strcmp(argv[1], "clone") == 0) {
		clone(argv);
	} else if (strcmp(argv[1], "init") == 0) {
		init();
	} else if (strcmp(argv[1], "upload") == 0) {
		upload(argc, argv);
	} else if (strcmp(argv[1], "help") == 0) {
		help(argc, argv);
	} else if (strcmp(argv[1], "job")==0) {
		job(argc, argv);
	} else if (strcmp(argv[1], "uninit")==0) {
		uninit();
	} else {
		printf("Error: '%s' is not a valid command.\n", argv[1]);
	}
	return 0;
}
